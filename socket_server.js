var ws = require("nodejs-websocket")
console.log("starting server")
// Scream server example: "hi" -> "HI!!!"
var latestData = {};
var monitorConnection;
var server = ws.createServer(function (conn) {
    // text received do this
    conn.on("text", function (str) {
     
      console.log('received' + str);
      var msg = JSON.parse(str);
      
      if(msg.id == 'geekymartianboard'){
        latestData = msg.tmp;
        console.log('latest temp set');
        if(monitorConnection){
          monitorConnection.sendText('{"tmp":"'+latestData+'"}')
        }
      }
      
      if(msg.id == 'outpostMonitor'){
        monitorConnection = conn;
      }

    })  

    // error
    conn.on("error", function () {
      console.log('error, probably somebody disconnecting');
    })  



    conn.on("close", function (code, reason) {
        console.log("Connection closed")
    })  
}).listen(8888,'0.0.0.0')